jupyter_client
ipython
ipykernel
pathlib
nbconvert
plotly
matplotlib
scipy
wikitables
pandas
jupytext
python-markdown-math
mkdocs-material
