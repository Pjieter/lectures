The following people contributed to making of these lecture notes:

* Anton Akhmerov
* Bas Nijholt
* Iacopo Bertelli
* Joana Fraxanet Morales
* Kevin Choi
* Piotr Benedysiuk
* Sander Otte
* Sathish Kumar RK
* Toeno van der Sar
* Hugo Kerstens
* Bowy La Riviere
* Matthias Flor
* Radoica Draškić
* Lex Huismans
* Gian de Bruin

<!--
Execute
git shortlog -s | sed -e "s/^ *[0-9\t ]*//"| xargs -i sh -c 'grep -q "{}" AUTHORS.md || echo "{}"'

To check if any authors are missing from this list.
 -->
