---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.0'
      jupytext_version: 1.0.2
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

```python tags=["initialize"]
import matplotlib.pyplot as plt

import numpy as np
from math import sqrt

from common import draw_classic_axes, configure_plotting

import plotly.offline as py
import plotly.graph_objs as go

configure_plotting()

py.init_notebook_mode(connected=True)
```

# Lecture 9 – Crystal structure

_based on chapter 12 of the book_  

!!! summary "Learning goals"

    After this lecture you will be able to:

    - Describe any crystal using crystallographic terminology, and interpret this terminology
    - Compute the volume filling fraction given a crystal structure
    - Determine the primitive, conventional, and Wigner-Seitz unit cells of a given lattice
    - Determine the Miller planes of a given lattice

### Crystal classification

- **_Lattice_**
    + periodic pattern of *lattice points*, which all have an identical view
    + lattice points are not necessarily the same as atom positions
    + there can be multiple atoms per lattice point
    + freedom of translation
    + multiple lattices with different point densities possible
- **_Lattice vectors_**
    + from lattice point to lattice point
    + $N$ linearly independent vectors for $N$ dimensions
    + integer combinations are also lattice vectors
    + not all sets of linearly independent lattice vectors provide full coverage of the lattice
- **_Unit cell_**
    + spanned by lattice vectors
    + has 4 corners in 2D, 8 corners in 3D
    + copying unit cell along lattice vectors gives full lattice
- **_Primitive unit cell_**
    + unit cell that contains exactly 1 lattice point
    + not always most practical choice
- **_Basis_**
    + only now do we care about the contents (i.e. atoms)
    + gives types and positions of atoms
    + two conventions: Cartesian coordinates or lattice coordinates

### Example: analyzing the graphene crystal structure

![](figures/graphite_mod.svg)

1. Choose origin (can be atom, not necessary)
2. Find other lattice points that are identical
3. Choose lattice vectors, either primitive (red) or not primitive (blue)
    - lengths of lattice vectors and angle(s) between them fully define the crystal lattice
    - for graphite: $|{\bf a}_1|=|{\bf a}_2|$ = 0.246 nm = 2.46 Å, $\gamma$ = 60$^{\circ}$
4. Specify basis (we use the lattice coordinates)
    - using ${\bf a}_1$ and ${\bf a}_2$: C$(0,0)$, C$\left(\frac{2}{3},\frac{2}{3}\right)$
    - using ${\bf a}_1$ and ${\bf a}_{2}'$: C$(0,0)$, C$\left(0,\frac{1}{3}\right)$, C$\left(\frac{1}{2},\frac{1}{2}\right)$, C$\left(\frac{1}{2},\frac{5}{6}\right)$

An alternative type of unit cell is the _Wigner-Seitz cell_: the collection of all points that are closer to one specific lattice point than to any other lattice point. You form this cell by taking all the perpendicular bisectrices or lines connecting a lattice point to its neighboring lattice points.

### Stacking of atoms
What determines what crystal structure a material adopts? $\rightarrow$ To good approximation, atoms are solid incrompressible spheres that attract each other. How will these organize?

We start with the densest possible packing in 2D:
![](figures/packing.svg)
Will the second layer go on sites A, B or C?

ABCABC stacking $\rightarrow$ _cubic close packed_, also known as _face centered cubic_ (fcc):

![](figures/stacking.svg)




```python
s = np.linspace(-1, 1, 100)
t = np.linspace(-1, 1, 100)
tGrid, sGrid = np.meshgrid(s, t)

x1 = 1 + sGrid + tGrid 
y1 = - sGrid 
z1 = - tGrid                  

surface1 = go.Surface(x=x1, y=y1, z=z1, opacity=0.7, showscale=False)


x2 = 1 + sGrid + tGrid 
y2 = 1 - sGrid 
z2 = -tGrid          

surface2 = go.Surface(x=x2, y=y2, z=z2, opacity=0.7, showscale=False)


Xn = np.tile(np.arange(0,2,1),4)
Yn = np.repeat(np.arange(0,2,1),4)
Zn = np.tile(np.repeat(np.arange(0,2,1),2),2)

Xn = np.hstack((Xn, Xn, Xn+0.5, Xn+0.5))
Yn = np.hstack((Yn, Yn+0.5, Yn+0.5, Yn))
Zn = np.hstack((Zn, Zn+0.5, Zn, Zn+0.5))

trace1=go.Scatter3d(x=Xn,
               y=Yn,
               z=Zn,
               mode = 'markers',
               marker = dict(
                        sizemode = 'diameter',
                        sizeref = 20,
                        size = 20,
                        color = 'rgb(255,255,255)',
                        line = dict(
                               color = 'rgb(0,0,0)',
                               width = 5
                               )
                       )
                     )
 
layout=go.Layout(showlegend = False,
                scene = dict(
                        aspectmode= 'cube',
                        xaxis=dict(
                        title= 'x[a]',
                        range= [0,1],
                        ticks='',
                        showticklabels=False
                        ),
                        yaxis=dict(
                        title= 'y[a]',
                        range= [0,1],
                        ticks='',
                        showticklabels=False
                        ),
                        zaxis=dict(
                        title= 'z[a]',
                        range= [0,1],
                        ticks='',
                        showticklabels=False
                        )
                    ))

data=[trace1, surface1, surface2]

fig=go.Figure(data=data, layout=layout)

py.iplot(fig, filename='FCC')
```

- One atom on the center of each side-plane: 'a die that always throws 1'
- Conventional unit cell $\neq$ primitive unit cell
- Cyclic ABC $\rightarrow$ all atoms identical $\rightarrow$ 1 atom per primitive unit cell
- Conventional cell: $8\times\frac{1}{8}+6\times\frac{1}{2}=1+3=4$ atoms

Examples of fcc crystals: Al, Cu, Ni, Pb, Au and Ag.

### Filling factor
Filling factor = # of atoms per cell $\times$ volume of 1 atom / volume of cell

$=\frac{4\times\frac{4}{3}\pi R^3}{a^3}=\frac{1}{6}\sqrt{2}\pi\approx 0.74$, where we used $\sqrt{2}a=4R$.

Compare this to _body centered cubic_ (bcc), which consists of a cube with atoms on the corners and one atom in the center (Fig. 12.20 of the book): filling factor = 0.68. Examples of bcc crystals: Fe, Na, W, Nb.

Question: is 74% the largest possible filling factor? $\rightarrow$ Kepler conjecture (1571 – 1630). Positive proof by Hales _et al._ in 1998!

Crystal structures with an fcc lattice:

1. ionic crystals are usually fcc. E.g. NaCl (Fig. 12.20 )
2. zincblende & diamond crystals (Fig. 12.20)

ABABAB stacking $\rightarrow$ _hexagonally close-packed_ (hcp), e.g. Co, Zn. In this case there is no cubic symmetry (Fig. 1.11).

### Miller planes
We start with a simple cubic lattice:

![](figures/cubic_mod.svg)

$|{\bf a}_1|=|{\bf a}_2|=|{\bf a}_3|\equiv a$ (_lattice constant_)

The plane designated by Miller indices $(u,v,w)$ intersects lattice vector ${\bf a}_1$ at $\frac{|{\bf a}_1|}{u}$, ${\bf a}_2$ at $\frac{|{\bf a}_2|}{v}$ and ${\bf a}_3$ at $\frac{|{\bf a}_3|}{w}$.

![](figures/miller.svg)

Miller index 0 means that the plane is parallel to that axis (intersection at "$\frac{|{\bf a}_3|}{0}=\infty$"). A bar above a Miller index means intersection at a negative coordinate.

If a crystal is symmetric under $90^\circ$ rotations, then $(100)$, $(010)$ and $(001)$ are physically indistinguishable. This is indicated with $\{100\}$. $[100]$ is a vector. In a cubic crystal, $[100]$ is perpendicular to $(100)$ $\rightarrow$ proof in problem set.

## Summary
* We introduced several important concepts that allow us to describe crystal structure: lattice, lattice vectors, basis, primitive & conventional unit cells, and Miller planes.
* We introduced several common lattices: simple-cubic, FCC, and BCC (3D), triangular (2D).
* We discussed how to compute a filling factor.
* A crystal is constructed by by placing the basis at each point in a lattice.

## Exercises

### Exercise 1: Diatomic crystal

Consider the following two-dimensional diatomic crystal:

```python
y = np.repeat(np.arange(0,8,2),4)
x = np.tile(np.arange(0,8,2),4)
plt.figure(figsize=(5,5))
plt.axis('off')

plt.plot(x,y,'ko', markersize=15)
plt.plot(x+1,y+1, 'o', markerfacecolor='none', markeredgecolor='k', markersize=15);
```

1. What is the definition of a primitive unit cell? Sketch a Wigner-Seitz unit cell and two other possible primitive unit cells of the crystal. 
2. If the distance between the filled cirles is $a=0.28$ nm, what is the volume of the primitive unit cell? How would this volume change if all the empty circles and the filled circles were identical?
3. Write down one set of primitive lattice vectors and the basis for this crystal.
4. Imagine expanding the lattice into the perpendicular direction $z$. We can define a new three-dimensional crystal by considering a periodic structure in the $z$ direction, where the filled circles have been displaced by $\frac{a}{2}$ from the empty circles. The following figure shows the new arrangement of the atoms. What lattice do we obtain? Write down the basis of the three-dimensional crystal and give an example of this type of crystal. 

```python
x = np.tile(np.arange(0,2,1),4)
y = np.repeat(np.arange(0,2,1),4)
z = np.tile(np.repeat(np.arange(0,2,1),2),2)

trace1 = go.Scatter3d(
    x = x,
    y = y,
    z = z,
    mode = 'markers',
    marker = dict(
        sizemode = 'diameter',
        sizeref = 20,
        size = 20,
        color = 'rgb(255, 255, 255)',
        line = dict(
          color = 'rgb(0,0,0)',
          width = 5
        )
        )
)

trace2 = go.Scatter3d(
    x = [0.5],
    y = [0.5],
    z = [0.5],
    mode = 'markers',
    marker = dict(
        sizemode = 'diameter',
        sizeref = 20,
        size = 20,
        color = 'rgb(0,0,0)',
        line = dict(
          color = 'rgb(0,0,0)',
          width = 5
        )
        )
)

data=[trace1, trace2]

layout=go.Layout(showlegend = False,
                scene = dict(
                        xaxis=dict(
                        title= 'x[a]',
                        ticks='',
                        showticklabels=False
                        ),
                        yaxis=dict(
                        title= 'y[a]',
                        ticks='',
                        showticklabels=False
                        ),
                        zaxis=dict(
                        title= 'z[a]',
                        ticks='',
                        showticklabels=False
                        )
                    ))


fig=go.Figure(data=data, layout=layout)
py.iplot(fig, show_link=False)
```

5. If we consider all atoms to be the same, what lattice do we obtain? Give an example of this type of crystal.
6. Compute the filling factor of the three-dimensional crystal. Choose the radius of the atoms to be such that the nearest neighbouring atoms just touch.

### Exercise 2: Diamond lattice

Consider a the [diamond crystal structure](https://en.wikipedia.org/wiki/Diamond_cubic) structure. The following illustration shows the arrangement of the carbon atoms in a conventional unit cell.

```python
Xn = np.tile(np.arange(0,2,1),4)
Yn = np.repeat(np.arange(0,2,1),4)
Zn = np.tile(np.repeat(np.arange(0,2,1),2),2)

Xn = np.hstack((Xn, Xn, Xn+0.5, Xn+0.5))
Yn = np.hstack((Yn, Yn+0.5, Yn+0.5, Yn))
Zn = np.hstack((Zn, Zn+0.5, Zn, Zn+0.5))

Xn = np.hstack((Xn, Xn+1/4))
Yn = np.hstack((Yn, Yn+1/4))
Zn = np.hstack((Zn, Zn+1/4))

Xe=[]
Ye=[]
Ze=[]

num_atoms = len(Xn)
for i in range(num_atoms):
    for j in np.arange(i+1, num_atoms, 1):
        pos1 = np.array([Xn[i], Yn[i], Zn[i]])
        pos2 = np.array([Xn[j], Yn[j], Zn[j]])
        if np.linalg.norm(pos1-pos2) == sqrt(3)/4:
            Xe+=[Xn[i], Xn[j], None]
            Ye+=[Yn[i], Yn[j], None]  
            Ze+=[Zn[i], Zn[j], None]  
    
trace1=go.Scatter3d(x=Xe,
               y=Ye,
               z=Ze,
               mode='lines',
               line=dict(color='rgb(0,0,0)', width=3),
               hoverinfo='none'
                )

trace2=go.Scatter3d(x=Xn,
               y=Yn,
               z=Zn,
               mode = 'markers',
               marker = dict(
                        sizemode = 'diameter',
                        sizeref = 20,
                        size = 20,
                        color = 'rgb(255,255,255)',
                        line = dict(
                               color = 'rgb(0,0,0)',
                               width = 5
                               )
                       )
                     )
 
layout=go.Layout(showlegend = False,
                scene = dict(
                        xaxis=dict(
                        title= 'x[a]',
                        range= [0,1],
                        ticks='',
                        showticklabels=False
                        ),
                        yaxis=dict(
                        title= 'y[a]',
                        range= [0,1],
                        ticks='',
                        showticklabels=False
                        ),
                        zaxis=dict(
                        title= 'z[a]',
                        range= [0,1],
                        ticks='',
                        showticklabels=False
                        )
                    ))

data=[trace1, trace2]

fig=go.Figure(data=data, layout=layout)

py.iplot(fig, filename='Diamond')
```

The side of the cube is $ a = 0.3567$ nm. 

1. How is this crystal structure related to the fcc lattice? Compute the basis and one set of primitive lattice vectors.
2. Determine the number of atoms in the primitive unit cell and compute its volume. 

    ??? hint

        Use the primitive lattice vectors you found in (2.1).
        
3. Determine the number of atoms in the conventional unit cell and compute its volume.
4. Using an image of the three-dimensional crystal, determine the number of nearest neighbours for each atom and write down the vectors connecting the nearest neighbours. Which is the nearest neighbour distance?
5. Compute the filling factor. Choose the radius of the atoms to be such that the neighbouring atoms just touch.

### Exercise 3: Directions and Spacings of Miller planes
*(adapted from ex 13.3 of "The Oxford Solid State Basics" by S.Simon)*

1. Explain what is meant by the terms Miller planes and Miller indices.
2. Consider a cubic crystal with one atom in the basis and a set of orthogonal primitive lattice vectors $a\hat{x}$, $a\hat{y}$ and $a\hat{z}$. Show that the direction $[hkl]$ in this crystal is normal to the planes with Miller indices $(hkl)$.
3. Show that this is not true in general. Consider for instance an orthorhombic crystal, for which the primitive lattice vectors are still orthogonal but have different lengths. 
4. Any set of Miller indices corresponds to a family of planes separated by a distance $d$. Show that the spacing $d$ of the $(hkl)$ set of planes in a cubic crystal with lattice parameter $a$ is $d = \frac{a}{\sqrt{h^2 + k^2 + l^2}}$.

    ??? hint

        Recall that a family of lattice planes is an infinite set of equally separated parallel planes which taken all together contain all points of the lattice.

        Try computing the distance between the plane that contains the site $(0,0,0)$ of the conventional unit cell and a plane defined by the $(hkl)$ indices.

