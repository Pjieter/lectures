# Solutions for Sommerfeld model exercises

### Exercise 1: potassium

1.

Alkali metals mostly have a spherical fermi surface. Their energy depends only on the magnitude of the Fermi wavevector.

2.

Refer to the lecture notes.

3.

Electrons are fermions and obeys pauli exclusion principle. As electrons cannot occupy the same state, they are forced to occupy higher energy states resulting in high Fermi energy and high Fermi temperature.

4.

$$
n = \frac{N}{V} = 2\frac{1}{(2\pi)^3}\frac{4}{3}\pi k_f^3
$$

5.

$$
n = \frac{\rho N_A Z}{M}
$$

where $\rho$ is the density, $N_A$ is the Avogadro's constant, $M$ is molar mass and $Z$ is the valence of potassium atom.

Comparing total and free electron density, only few electrons are available for conduction which is roughly 1 free electron per potassium atom.

### Exercise 2: the n-dimensional free electron model

1.

Distance between nearest k-points is $\frac{2\pi}{L}$ and their density across n-dimensions is $(\frac{L}{2\pi})^n$.

2.

$$
g_{1D}(k)dk = \left(\frac{L}{2\pi}\right) 2 .dk
$$

Factor 2 is due to positive and negative k-points having equal energy.

$$
g_{2D}(k)dk = \left(\frac{L}{2\pi}\right)^2 2\pi k . dk
$$

$$
g_{3D}(k)dk = \left(\frac{L}{2\pi}\right)^3 4\pi k^2 . dk
$$

$4\pi k^2$ is the volume of spherical shell enclosed between k and k + dk.

3, 4.

$$
g(k)dk = \left(\frac{L}{2\pi}\right)^n S_{n-1}(k)dk = \left(\frac{L}{2\pi}\right)^n \frac{2\pi^{\frac{n}{2}}k^{n-1}}{\Gamma(\frac{n}{2})}dk
$$

5.

$$
2g(k)dk = g(E)dE
$$
$$
g(E)=2g(k(E))\frac{dk}{dE}
$$

$$
g(E) = \frac{2}{\Gamma(\frac{n}{2})}\left(\frac{L}{\hbar}\sqrt{\frac{m_e}{2\pi}}\right)^n (E)^{\frac{n}{2}-1}
$$

6.

$$
N = \int_{0}^{\infty}g(E)f(E)dE = \frac{2}{\Gamma(\frac{n}{2})}\left(\frac{L}{\hbar}\sqrt{\frac{m_e}{2\pi}}\right)^n\int_{0}^{\infty}\frac{(E)^{\frac{n}{2}-1}}{e^{\frac{E-\mu}{k_BT}}+1}dE
$$

Total energy: $E = \int_{0}^{\infty}g(E)f(E)EdE $

### Exercise 3: a hypothetical material

1.

$$
E = \int_{0}^{\infty}\epsilon g(\epsilon)f(\epsilon)d\epsilon = 2.10^{10}\int_{0}^{\infty}\frac{\epsilon^{\frac{3}{2}}}{e^\frac{\epsilon-5.2}{k_BT}+1}d\epsilon
$$

2.

Substitute T=0 in the integral expression for total energy to find the ground state energy.

3.

$$
\Delta E = \frac{\pi^2}{6}(k_B T)^2\frac{\partial}{\partial \epsilon}\left(\epsilon g(\epsilon)\right)\bigg|_{\epsilon=\epsilon _F}
$$

5.

$C_v = 1.6713.10^6 eV/K$

4, 6.

```python
mu = 5.2
kB = 8.617343e-5
T = 1000 #kelvin

import numpy as np

np.seterr(over='ignore')

# Fermi-Dirac distribution 
def f(E, T):
    return 1 / (np.exp((E - mu)/(kB*T)) + 1)

# Density of states
def g(E):
    return 2e10 * np.sqrt(E)

#integration function
def integral(E, T):
    return f(E, T)*g(E)*E

## Solve integral using midpoint rule
elements = np.linspace(0, 1e6, 1e7) #Free to choose the number of discrete steps
dE = np.sum(integral(elements, T))*((1e6 - 0)/len(elements)) - 0.8e10 * 5.2**(5./2)

dT = 0.001
dEplus = np.sum(integral(elements, 1000+dT))*((1e6 - 0)/len(elements)) - 0.8e10 * 5.2**(5./2)
dEmin = np.sum(integral(elements, 1000-dT))*((1e6 - 0)/len(elements)) - 0.8e10 * 5.2**(5./2)

CV = (dEplus - dEmin) / (2*dT);

print('dE = {:.4e} eV'.format(dE))
print('Cv = {:.4e} eV/K'.format(CV))
```
Check the source code written in python for solving integral using midpoint rule.

### Exercise 4: graphene

1.

```python
import numpy as np
import matplotlib.pyplot as plt
x = np.linspace(-1, 1, 100)
fig, ax = plt.subplots(figsize=(7, 5))
ax.plot(x, x, 'b')
ax.plot(x,-x, 'b')

ax.spines['left'].set_position('center')
ax.spines['bottom'].set_position(('data', 0.0))

# Eliminate upper and right axes
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')

ax.set_xticks([])
ax.set_yticks([])

ax.set_xlabel(r'$\mid \vec k \mid$', fontsize=14)
ax.set_ylabel('$\epsilon$', fontsize=18, rotation='horizontal')
ax.yaxis.set_label_coords(0.5,1)
ax.xaxis.set_label_coords(1.0, 0.49)
```

2.

In two dimensions: $g(k) dk = (2+2)\frac{Ak}{2\pi}dk$

$$
g(\epsilon)d\epsilon = \frac{2Ak}{\pi}dk
$$

Substituting $\epsilon(k) = \pm c\mid k\mid$,

$$
g(\epsilon) = \frac{2A\mid \epsilon \mid}{\pi c^2}
$$

3.

$g(\epsilon)$ vs $\epsilon$ is a linear plot. Here, the region marked by $k_B T$ is a triangle whose area gives the number of electrons that can be excited.

$$
n_{ex} = \frac{Ak_B^2T^2}{\pi c^2}
$$

$$
E(T) - E_0 = \frac{Ak_B^3T^3}{\pi c^2}
$$

4.

$$
C_v(T) = \frac{\partial E}{\partial T} = \frac{3Ak_B^3T^2}{\pi c^2}
$$

