# Solutions for Drude model exercises

### Exercise 1: Extracting quantities from basic Hall measurements

1.

Hall voltage is measured across the sample width. Hence,

$$
V_H = -\int_{0}^{W} E_ydy
$$

where $E_y = -v_xB$.

$R_{xy}$ = $-\frac{B}{ne}$, so it does not depend on the sample geometry.


2.

If hall resistance and charge density are known, magnetic field is calculated from $R_{xy} = -\frac{B}{ne}$.

As $V_x = -\frac{I_x}{ne}B$, smaller n ensures large hall voltages easy to measure.

3.

$$
R_{xx} = \frac{\rho_{xx}L}{W}
$$ 

where $ \rho_{xx} = \frac{m_e}{ne^2\tau}$. Therefore, scattering time ($\tau$) is known and $R_{xx}$ depend upon the sample geometry.

### Exercise 2: Motion of an electron in a magnetic and an electric field

1.

$$
m\frac{d\bf v}{dt} = -e(\bf v \times \bf B)
$$

Magnetic field affects only the velocities along x and y, i.e., $v_x(t)$ and $v_y(t)$ as they are perpendicular to it. Therefore, the equations of motion for the electron are

$$
\frac{dv_x}{dt} = \frac{ev_yB_z}{m}
$$

$$
\frac{dv_y}{dt} = -\frac{ev_xB_z}{m}
$$



2.

Compute $v_x(t)$ and $v_y(t)$ by solving the differential equations in 1. Calculate the positions $x(t)$ and $y(t)$ from $v_x(t)$ and $v_y(t)$ respectively with the initial conditions: $v_x=u_x$ and $v_y=0$. It results in

$$
(x(t) - x_0)^2 + (y(t) - y_0)^2 = \frac{u_x^2}{\omega_c^2}
$$

where $\omega_c = \frac{e\bf B}{m}$. This represents the circular motion.

$\omega_c$ is the characteristic frequency also called as *cyclotron* frequency. Intuition:    $\frac{mv^2}{r} = evB$ (centripetal force = Lorentz force due to magnetic field).

3.

Due to applied electric field $\bf E$ the equations of motion has an extra term,
$$
m\frac{d\bf v}{dt} = -e(\bf E + \bf v \times \bf B)
$$

Thus the differential equation for velocity differs,
$$
\frac{dv_x}{dt} = -\frac{eBv_y}{m}
$$

$$
\frac{dv_y}{dt} = \frac{eBv_x}{m} - \frac{eE}{m}
$$

Repeat steps followed in 2. to compute x(t) and y(t) after which the particle motion is given by,

$$
\left(x(t) - \frac{Et}{B}\right)^2 + \left(y(t) - \frac{E}{B\omega_c}\right)^2 = \left(\frac{E}{B\omega_c}\right)^2
$$

This represents the [cycloid](https://en.wikipedia.org/wiki/Cycloid#/media/File:Cycloid_f.gif) where the motion is along along x with velocity $\frac{E}{B}$.

4.

$$
m\left(\frac{d\bf v}{dt} + \frac{\bf v}{\tau}\right) = -e(\bf E + \bf v \times \bf B)
$$

### Exercise 3: Temperature dependence of resistance in the Drude model

1.

Find electron density from $n_e =   \frac{ZnN_A}{W} $ 

where *Z* is valence of copper atom, *n* is density, $N_A$ is Avogadro constant and *W* is atomic weight. Use $\rho$ from the lecture notes to calculate scattering time.

2.

$\lambda = \langle v \rangle\tau$

3.

Scattering time $\tau \propto \frac{1}{\sqrt{T}}$; $\rho \propto \sqrt{T}$

4.

In general, $\rho \propto T$ as the phonons in the system scales linearly with T (remember high temperature limit of Bose-Einstein factor becomes $\frac{kT}{\hbar\omega}$ leading to $\rho \propto T$). Inability to explain this linear dependence is a failure of the Drude model.

### Exercise 4: The Hall conductivity matrix and the Hall coefficient

1.

$\rho_{xx}$ is independent of B and

$\rho_{xy} \propto B$

2, 4.

Refer to the lecture notes

3.

$$
\sigma_{xx} \approx \frac{1}{1 + \rho_{xy}^2}
$$

$$
\sigma_{xy} \approx \frac{-\rho_{xy}}{1 + \rho_{xy}^2}
$$

This describes [Lorentzian](https://en.wikipedia.org/wiki/Spectral_line_shape#Lorentzian).
