```python tags=["initialize"]
from matplotlib import pyplot

import numpy as np

from common import draw_classic_axes, configure_plotting

configure_plotting()
```

_(based on chapter 4 of the book)_  

!!! success "Expected prior knowledge"

    Before the start of this lecture, you should be able to:

    - Write down the Fermi-Dirac distribution function.
    - Write down the Schrödinger equation and solve it in free space.
    - Apply periodic boundary conditions, and compute the density of states
      given a sufficiently simple dispersion relation.

!!! summary "Learning goals"

    After this lecture you will be able to:

    - calculate the electron density of states in 1D, 2D, and 3D using the Sommerfeld free-electron model.
    - express the number and energy of electrons in a system in terms of integrals over k-space.
    - use the Fermi distribution to extend the previous learning goal to finite T.
    - calculate the electron contribution to the specific heat of a solid.
    - describe central terms such as the Fermi energy, Fermi temperature, and Fermi wavevector.

## Electrons vs phonons

> Two electrons are sitting on a bench. Another one approaches and asks: "May I join you guys?"
> The first two immediately reply: "Who do you think we are? Bosons?"

Having learned the statistical properties of phonons and the [Debye model](2_debye_model.md), let us use these as a starting point for comparing with the electrons.
Here is a table comparing the most important properties of electrons and phonons:

|   | Phonons | Electrons |
| - | - | - |
| Governed by | Wave equation | Schrödinger equation |
|  | $ d² δ\mathbf{r} / dt² = v²∇²δ\mathbf{r}$ | $ i ħdψ/dt = -ħ²∇²ψ/2m$ |
| Dispersion relation | $ω=v \|\mathbf{k}\|$ | $ε = ħ²k²/2m$ |
| Statistics | Bose-Einstein | Fermi-Dirac |
| $n =$ | $1/[\exp(βE) - 1]$ | $1/[\exp(β[E - μ]) + 1]$ |
| Number per $\mathbf{k}$ | 3 (polarization) | 2 (spin) |
| Total number | temperature-dependent | as many as there are |

On the one hand, there are important differences, but on the other hand there are a lot of similarities.

* The solutions of the equation of motion are still plane waves $ψ ∝ \exp(i\mathbf{k}\mathbf{r})$.
* The periodic boundary conditions work exactly the same: $k_{x,y,z}=…, \frac{-4\pi}{L}, \frac{-2\pi}{L}, 0, \frac{2\pi}{L}, \frac{4\pi}{L}, …$
* The density of $k$-states per unit volume in the reciprocal space is the same: $(L/2π)³$.
* The expression for the total energy of the system is *very similar*:  
  $E_\textrm{total} = 2_s (L/2π)³∫ ε(\mathbf{k}) n_F[ε(\mathbf{k})]d³\mathbf{k}.$

With this comparison we have everything to analyze the electron behavior.

## Fermi sea

For a warm-up let us imagine what happens in 1D, when we only have a few electron states available.

```python
kf = 3;
extrapol = 1.1;
ks = np.arange(-kf, kf+1);
kcont = np.linspace(-extrapol*kf, extrapol*kf, 200);

Edis = ks**2;
Econt = kcont**2;

fig = pyplot.figure();
#fig.set_size_inches(2, 2)
ax = fig.add_subplot(111);
ax.plot(kcont, Econt);
ax.plot(ks, Edis, 'k.', markersize=10);
for i in range(2*kf + 1):
    ax.plot([ks[i], ks[i]], [0.0, Edis[i]], 'k:');
ax.set_xlim(-3.75, 3.75);
ax.set_ylim(0.0, 11);

ax.set_xlabel(r"$k \enspace \left[ \frac{2 \pi}{L} \right]$");
ax.set_ylabel(r"$ε$");

ax.set_xticklabels([""] + ks.tolist() + [""]);
ax.set_yticks([]);

draw_classic_axes(ax, xlabeloffset=.6);
```

At $T=0$ the states with zero energy get completely filled up because the electrons cannot disappear.

![](figures/fermi_circle_periodic.svg)

## Density of states

Our first goal is to compute the density of states, which we will do now using a more explicit algorithm.
By definition density of states is the number of states per energy interval.
Therefore if we compute the *total* number of states $N(ε)$ with energy lower than $ε$, then $g(ε) = dN(ε)/dε$.

Let us apply this to the 3D case for a start.
Assuming three dimensions and spherical symmetry (the dispersion in the free electron model is isotropic), we find
$$
N=2\left(\frac{L}{2\pi}\right)^3\int\mathrm{d}{\bf k}=2 \left(\frac{L}{2\pi}\right)^34\pi\int k^2\mathrm{d}k=\frac{V}{\pi^2}\int k^2\mathrm{d}k,
$$
where the factor 2 is due to spin, and $\left(\frac{L}{2\pi}\right)^3$ is once again the density of points in k-space.

Using $k=\frac{\sqrt{2mε}}{\hbar}$ and $\mathrm{d}k=\frac{1}{\hbar}\sqrt{\frac{m}{2ε}}\mathrm{d}ε$ we can rewrite this as:
$$
N=\frac{V}{\pi^2}\int\frac{2mε}{\hbar^3}\sqrt{\frac{m}{2ε}}\mathrm{d}ε=\frac{Vm^{3/2}}{\pi^2\hbar^3}\int\sqrt{2ε}\ \mathrm{d}ε
$$

So we find for the density of states:
$$
g(ε)=\frac{ \mathrm{d}N}{ \mathrm{d}ε}=\frac{Vm^{3/2}\sqrt{2ε}}{\pi^2\hbar^3}\propto\sqrt{ε}
$$

```python
E = np.linspace(0, 2, 500)
fig, ax = pyplot.subplots()

ax.plot(E, np.sqrt(E))

ax.set_ylabel(r"$g(ε)$")
ax.set_xlabel(r"$ε$")
draw_classic_axes(ax, xlabeloffset=.2)
```

Similarly,

- For 1D: $g(ε) = \frac{2 L}{\pi} \frac{ \mathrm{d}k}{ \mathrm{d}ε} \propto 1/\sqrt{ε}$
- For 2D: $g(ε) = \frac{k L^2}{\pi} \frac{ \mathrm{d}k}{ \mathrm{d}ε} \propto \text{constant}$


Given the number of electrons in a system, we can now fill up these states starting from the lowest energy until we run out of electrons, at which point we reach the _Fermi energy_.

## Fermi energy, Fermi wavevector, Fermi wavelength

At $T=0$, the total number of electrons is given by the integral over the density of states up to the _Fermi energy_ $ε_\mathrm{F}$:

$$
N=\int_0^{ε_\mathrm{F}}g(ε)\mathrm{d}ε \overset{\mathrm{3D}}{=} \frac{V}{3\pi^2\hbar^3}(2mε_\mathrm{F})^{3/2}.
$$
Note that $N$ now denotes the total number of electrons in the system.

Alternatively, we can express $N$ as an integral over k-space up to the _Fermi wavenumber_, which is the wavenumber associated with the Fermi energy $k_\mathrm{F}=\sqrt{2mε_\mathrm{F}}/\hbar$

$$
N \overset{\mathrm{3D}}{=} 2\frac{L^3}{(2\pi)^3}\int_0^{k_\mathrm{F}} 4\pi k^2\mathrm{d}k= 2\frac{V}{(2\pi)^3} \frac{4}{3}\pi k_\mathrm{F}^3
$$

These equations allow us to relate $ε_\mathrm{F}$ and $k_\mathrm{F}$ to the electron density $N/V$:

$$
ε_\mathrm{F}=\frac{\hbar^2}{2m}\left( 3\pi^2\frac{N}{V} \right)^{2/3}, \mathrm{and} \quad k_\mathrm{F}=\left( 3\pi^2\frac{N}{V} \right)^{1/3}.
$$

From the last equation it follows that the _Fermi wavelength_ $\lambda_\mathrm{F}\equiv 2\pi/k_\mathrm{F}$ is on the order of the atomic spacing for typical free electron densities in metals.

Example: For copper, the Fermi energy is ~7 eV. It would take a temperature of $\sim 70 000$K for electrons to gain such energy through a thermal excitation! The _Fermi velocity_ $v_\mathrm{F}=\frac{\hbar k_\mathrm{F}}{m}\approx$ 1750 km/s $\rightarrow$ electrons run with a significant fraction of the speed of light, only because lower energy states are already filled by other electrons.


```python
kf = 3.0;
extrapol = 4.0/3.0;
kfilled = np.linspace(-kf, kf, 500);
kstates = np.linspace(-extrapol*kf, extrapol*kf, 500);

Efilled = kfilled**2;
Estates = kstates**2;

fig = pyplot.figure();
ax = fig.add_subplot(111);
ax.fill_between(kfilled, Efilled, kf*kf, alpha=0.5);
ax.plot([kf, kf], [0.0, kf*kf], 'k:');
ax.plot(kstates, Estates, 'k--');
ax.plot(kfilled, Efilled, linewidth=4);
ax.axhline(kf*kf, linestyle="dotted", color='k');

ax.set_xticks([kf]);
ax.set_yticks([kf*kf + 0.4]);
ax.set_xticklabels([r"$k_F$"]);
ax.set_yticklabels([r"$ε_F$"]);

ax.set_xlabel(r"$k$");
ax.set_ylabel(r"$ε$");

ax.set_xlim(-kf*extrapol, kf*extrapol);
ax.set_ylim(0.0, kf*kf*extrapol);
draw_classic_axes(ax, xlabeloffset=.6);
```

The bold line represents all filled states at $T=0$. This is called the _Fermi sea_.

New concept: _Fermi surface_ = all points in k-space with $ε=ε_\mathrm{F}$. For free electrons in 3D, the Fermi surface is the surface of a sphere.

![](figures/transport.svg)

The orange circle represents the Fermi surface at finite current $\rightarrow$ this circle will shift only slightly before the electrons reach terminal velocity $\rightarrow$ all transport takes place near the Fermi surface.


## Finite temperature, heat capacity
We now extend our discussion to $T>0$ by including a temperature dependent occupation function $n_F(ε,T)$ into our expression for the total number of electrons:

$$
N=\int_0^\infty n_F(ε,T)g(ε)\mathrm{d}ε,
$$
where the probability for a certain electron state to be occupied is given by the Fermi-Dirac distribution
$$
n_F(ε,T)=\frac{1}{ \mathrm{e}^{(ε-\mu)/k_\mathrm{B}T}+1}
$$

```python
fig = pyplot.figure()
ax = fig.add_subplot(1,1,1)
xvals = np.linspace(0, 2, 200)
mu = .75
beta = 20
ax.plot(xvals, xvals < mu, ls='dashed', label='$T=0$')
ax.plot(xvals, 1/(np.exp(beta * (xvals-mu)) + 1),
        ls='solid', label='$T>0$')
ax.set_xlabel(r'$ε$')
ax.set_ylabel(r'$n_F(ε, T)$')
ax.set_yticks([0, 1])
ax.set_yticklabels(['$0$', '$1$'])
ax.set_xticks([mu])
ax.set_xticklabels([r'$\mu$'])
ax.set_ylim(-.1, 1.1)
ax.legend()
draw_classic_axes(ax)
pyplot.tight_layout()
```
where the chemical potential $\mu=ε_\mathrm{F}$ if $T=0$. Typically $ε_\mathrm{F}/k_\mathrm{B}$~70 000 K (~7 eV), whereas room temperature is only 300 K (~30 meV). Therefore, thermal smearing occurs only very close to the Fermi energy.

Having included the temperature dependence, we can now calculate the electronic contribution to the heat capacity $C_\mathrm{V,e}$.

```python
E = np.linspace(0, 2, 500)
fig, ax = pyplot.subplots()
ax.plot(E, np.sqrt(E), linestyle='dashed')
ax.text(1.7, 1.4, r'$g(ε)\propto \sqrt{ε}$', ha='center')
ax.fill_between(E, np.sqrt(E) * (E < 1), alpha=.3)

n = np.sqrt(E) / (1 + np.exp(20*(E-1)))
ax.plot(E, n)
ax.fill_between(E, n, alpha=.5)
w = .17
ax.annotate(s='', xy=(1, 1), xytext=(1-w, 1),
            arrowprops=dict(arrowstyle='<->', shrinkA=0, shrinkB=0))
ax.text(1-w/2, 1.1, r'$\sim k_BT$', ha='center')
ax.plot([1-w, 1+w], [1, 0], c='k', linestyle='dashed')
ax.annotate(s='', xy=(1, 0), xytext=(1, 1),
            arrowprops=dict(arrowstyle='<->', shrinkA=0, shrinkB=0))
ax.text(1.2, .7, r'$g(ε_F)$', ha='center')
ax.set_xticks([1])
ax.set_xticklabels([r'$ε_F$'])

ax.set_ylabel(r"$g(ε)$")
ax.set_xlabel(r"$ε$")
draw_classic_axes(ax, xlabeloffset=.2)
```

We will estimate $C_\mathrm{V,e}$ and derive its scaling with temperature using the triangle method depicted in the figure. A finite temperature causes electrons in the top triangle to be excited to the bottom triangle. Because the base of this triangle scales with $k_\mathrm{B}T$ and its height with $ g(ε_\mathrm{F})$, it follows that the number of excited electrons $N_\mathrm{exc} \approx g(ε_\mathrm{F})k_\mathrm{B}T$ (neglecting pre-factors of order 1).

These electrons gain $k_\mathrm{B}T$ of energy, so the total extra energy is

$$
E(T)-E(0)=N_\mathrm{exc}k_\mathrm{B}T\approx g(ε_\mathrm{F})k_\mathrm{B}^2T^2.
$$

Therefore, the heat capacity is given by

$$
C_\mathrm{V,e}=\frac{ \mathrm{d}E}{ \mathrm{d}T} \approx 2 g(ε_\mathrm{F})k_\mathrm{B}^2T=\ ...\ =3 Nk_\mathrm{B}\frac{T}{T_\mathrm{F}}\propto T,
$$
where we used $N=\frac{2}{3}ε_\mathrm{F}g(ε_\mathrm{F})$ and we defined the _Fermi temperature_ $T_\mathrm{F}=\frac{ε_\mathrm{F}}{k_\mathrm{B}}$.

How does $C_\mathrm{V,e}$ relate to the phonon contribution $C_\mathrm{V,p}$?

- At room temperature, $C_\mathrm{V,p}=3Nk_\mathrm{B}\gg C_\mathrm{V,e}$
- Near $T=0$, $C_\mathrm{V,p}\propto T^3$ and $C_\mathrm{V,e}\propto T$ $\rightarrow$ competition.

## Useful trick: scaling of $C_V$

Behavior of $C_\mathrm{V}$ can be very quickly memorized or understood using the following mnemonic rule

> Particles in within an energy range ~$kT$ become thermally excited, and each carries extra energy $kT$.

#### Example 1: electrons

$g(ε_\mathrm{F})$ roughly constant ⇒ total energy in the thermal state is $T \times [T\times g(ε_\mathrm{F})]$ ⇒ $C_\mathrm{V} \propto T$.

#### Example 2: graphene with $E_F=0$ (midterm 2018)

$g(ε) \propto ε$ ⇒ total energy is $T \times T^2$ ⇒ $C_\mathrm{V} \propto T^2$.

#### Example 3: phonons in 3D at low temperatures.

$g(ε) \propto ε^2$ ⇒ total energy is $T \times T^3$ ⇒ $C_\mathrm{V} \propto T^3$.


## Conclusions
  1. The Sommerfeld free electron model treats electrons as waves with dispersion $ε=\frac{\hbar^2k^2}{2m}$.
  2. The density of states (DOS) can be derived from the dispersion relation. This procedure is general, and analogous to e.g. that for phonons (see lecture 2 - Debye model).
  3. The Fermi-Dirac distribution describes the probability an electron state is occupied.
  4. The free-electron heat capacity is linear with $T$.
  5. The scaling of heat capacity with $T$ can be derived quickly by estimating the nr of particles in an energy range $k_\mathrm{B}T$, using the DOS.

## Exercises

### Warm-up questions

1. List the differences between electrons and phonons from your memory.
2. Write down the expression for the total energy of particles with the density of states $g(E)$ and the occupation number $n(E, T)$.
3. Explain what happens if a material is heated up to its Fermi temperature (assuming that materials where it is possible exist).

### Exercise 1: potassium
The Sommerfeld model provides a good description of free electrons in alkali metals such as potassium, which has a Fermi energy of 2.12 eV (data from Ashcroft, N. W. and Mermin, N. D., Solid State Physics, Saunders, 1976.).

1. Check the [Fermi surface database](http://www.phys.ufl.edu/fermisurface/). Explain why potassium and (most) other alkali metals can be described well with the Sommerfeld model.
2. Calculate the Fermi temperature, Fermi wave vector and Fermi velocity for potassium.
3. Why is the Fermi temperature much higher than room temperature?
4. Calculate the free electron density in potassium.
5. Compare this with the actual electron density of potassium, which can be calculated by using the density, atomic mass and atomic number of potassium. What can you conclude from this?

### Exercise 2: the $n$-dimensional free electron model.
In the lecture, it has been explained that the density of states (DOS) of the free electron model is proportional to $1/\sqrt{\epsilon}$ in 1D, constant in 2D and proportional to $\sqrt{\epsilon}$ in 3D. In this exercise, we are going to derive the DOS of the free electron model for an arbitrary number of dimensions.
Suppose we have an $n$-dimensional hypercube with length $L$ for each side and contains free electrons.

1. Assuming periodic boundary conditions, what is the distance between nearest-neighbour points in $\mathbf{k}$-space? What is the density of $\mathbf{k}$-points in n-dimensional $\mathbf{k}$-space?
2. The number of $\mathbf{k}$-points with a magnitude between $k$ and $k + dk$ is given by $g(k)dk$. Using the answer for 1, find $g(k)$ for 1D, 2D and 3D.
3. Now show that $g(k)$ for $n$ dimensions is given by
  $$g(k) = \frac{1}{\Gamma(n/2)} \left( \frac{L }{ \sqrt{\pi}} \right)^n \left( \frac{k}{2} \right)^{n-1},$$ where $\Gamma(z)$ is the [gamma function](https://en.wikipedia.org/wiki/Gamma_function).

	??? hint
		You will need the area of an $n$-dimensional sphere and this can be found on [Wikipedia](https://en.wikipedia.org/wiki/N-sphere#Volume_and_surface_area) (blue box on the right).

4. Check that this equation is consistent with your answers in 2.

	??? hint
		Check [Wikipedia](https://en.wikipedia.org/wiki/Particular_values_of_the_gamma_function) to find out how to deal with half-integer values in the gamma function.

5. Using the expression in 3, calculate the DOS (do not forget the spin degeneracy).
6. Give an integral expression for the total number of electrons and for their total energy in terms of the DOS, the temperature $T$ and the chemical potential $\mu$ (_you do not have to work out these integrals_).
7. Work out these integrals for $T = 0$.

### Exercise 3:  a hypothetical material
A hypothetical metal has a Fermi energy $\epsilon_F = 5.2 \, \mathrm{eV}$ and a DOS per unit volume $g(\epsilon) =  2 \times 10^{10} \, \mathrm{eV}^{-\frac{3}{2}} \sqrt{\epsilon}$.

1. Give an integral expression for the total energy of the electrons in this hypothetical material in terms of the DOS $g(\epsilon)$, the temperature $T$ and the chemical potential $\mu = \epsilon_F$.
2. Find the ground state energy at $T = 0$.
3. In order to obtain a good approximation of the integral for non-zero $T$, one can make use of the [Sommerfeld expansion](https://en.wikipedia.org/wiki/Sommerfeld_expansion). Using this expansion, find the difference between the total energy of the electrons for $T = 1000 \, \mathrm{K}$ with that of the ground state.
4. Now, find this difference in energy by calculating the integral found in 1 numerically. Compare your result with 3.

	??? hint
		 You can do numerical integration in MATLAB with [`integral(fun,xmin,xmax)`](https://www.mathworks.com/help/matlab/ref/integral.html).

5. Calculate the heat capacity for $T = 1000 \, \mathrm{K}$ in eV/K.
6. Numerically compute the heat capacity by approximating the derivative of energy difference found in 4 with respect to $T$. To this end, make use of the fact that $$\frac{dy}{dx}=\lim_{\Delta x \to 0} \frac{y(x + \Delta x) - y(x - \Delta x)}{2 \Delta x}.$$ Compare your result with 5.

### Exercise 4: graphene
One of the most famous recently discovered materials is [graphene](https://en.wikipedia.org/wiki/Graphene), which consists of carbon atoms arranged in a 2D honeycomb structure. In this exercise, we will focus on the electrons in bulk graphene. Unlike in metals, electrons in graphene cannot be treated as 'free'. However, close to the Fermi level, the dispersion relation can be approximated by a linear relation:
$$ \epsilon(\mathbf{k}) = \pm c|\mathbf{k}|.$$ Note that the $\pm$ here means that there are two energy levels at a specified $\mathbf{k}$. The Fermi level is set at $\epsilon_F = 0$.

1. Make a sketch of the dispersion relation. What other well-known particles have a linear dispersion relation?
2. Using the dispersion relation and assuming periodic boundary conditions, derive an expression for the DOS of graphene. Your result should be linear with $|\epsilon|$. Do not forget spin degeneracy, and take into account that graphene has an additional two-fold 'valley degeneracy'.
3. At finite temperatures, assume that electrons close to the Fermi level (i.e. not more than $k_B T$ below the Fermi level) will get thermally excited, thereby increasing their energy by $k_B T$. Calculate the difference between the energy of the thermally excited state and that of the ground state $E(T)-E_0$. To do so, show first that the number of electrons that will get excited is given by $$n_{ex} = \frac{1}{2} g(-k_B T) k_B T.$$
4. Calculate the heat capacity $C_V$ as a function of the temperature $T$.
