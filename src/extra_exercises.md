### Exercise 1: anisotropic Drude model
Recall the Drude theory, which assumes that electrons (charge $-e$, mass $m_e$) move according to Newtonian mechanics, but have a scattering event after an average scattering time which sets the velocity to 0. Now consider a 3D anisotropic crystal that have different scattering times $\tau_x$, $\tau_y$, $\tau_z$ for electrons moving in the $x$, $y$, and $z$ direction respectively. First, suppose there is only an electric field $\mathbf{E}=(E_x,E_y,E_z)$ in the crystal.

1. Write down the equations of motion of the electrons for the $x$, $y$, and $z$ component separately.
2. In case of steady state, calculate the current density $\mathbf{j}$, given the electric field $\mathbf{E}$ and electron density $n$. What is the angle between $\mathbf{j}$ and $\mathbf{E}$?
3. We now apply a magnetic field $\mathbf{B}=(B_x, B_y, B_z)$ to the crystal. What extra term(s) should be added to the equations of motion found in 1? Write down the resistivity matrix $\hat{\rho}$ of the crystal.
4. Given a steady current $\mathbf{j}$ flowing through the crystal in the presence of the magnetic field $\mathbf{B}$, calculate the electric field $\mathbf{E}_{Hall}$ due to the Hall effect. Show that this field is perpendicular to the current.
5. Suppose that an electron initially moves in the $x$-direction and that $\tau_x \gg \tau_y$. Draw the trajectory of the electron in the $xy$-plane if there is no external electric field and a magnetic field in the positive $z$ direction strong enough to see bending.

### Exercise 2: acetylene
Consider an acetylene molecule given in the figure below, which consist of 2 carbon atoms (black) and 2 hydrogen atoms (white).

![acetylene](https://upload.wikimedia.org/wikipedia/commons/8/8b/Acetylene-CRC-IR-3D-balls.png)

??? info "source"
    By Ben Mills, own work, public domain, [link](https://commons.wikimedia.org/wiki/File:Acetylene-CRC-IR-3D-balls.png).

Assume that the atoms can only move along the length of the molecule. Let the mass of the carbon atom be $m_C$ and the mass of the hydrogen atom be $m_H$. Futhermore, suppose that the sping constant between the two carbon atoms is $\kappa_{CC}$ and the spring constant between a carbon atom and hydrogen atom is $\kappa_{CH}$.

1. Write down the equation of motion for each of the atoms in the acetylene molecule.
2. What trial solution do you need to use to find normal modes with frequency $\omega$ of the acetylene molecule? Plug this trial solution in the equations of motion and write down the equations as an eigenvalue problem.
3. In order to solve this eigenvalue problem, we can make use the mirror symmetry of the acetylene molecule. What matrix operation does correspond to this mirror symmetry? Show that this matrix commutes with the one in the eigenvalue problem obtained in 2. What can be said about matrices that commute?
4. Using this mirror symmetry, find the eigenvectors of the eigenvalue problem obtained in 2. Make a visual sketch of the normal modes of acetylene.
5. What eigenfrequency $\omega$ do each of these modes have?

### Exercise 3: tight binding model in 3D
Suppose we have a 3D monatomic crystal arranged in a cubic lattice with lattice constant $a$. Suppose that these atoms have an onsite energy $\varepsilon$ and that there is nearest neighbor hopping of $-t$ between the atoms. We express the LCAO of the crystal as $$\left|\Psi\right>=\sum_{\alpha,\beta,\gamma} \varphi_{\alpha,\beta,\gamma} \left|\alpha,\beta,\gamma\right>,$$ where $\left|\alpha,\beta,\gamma\right>$ is the atomic orbital at position $(x,y,z)=(\alpha a,\beta a, \gamma a)$.

1. Using this LCAO and the Schrödinger equation, derive the tight binding equation.
3. Using the trial solution $$\varphi_{n,m} = A \exp[-i (k_x \alpha + k_y \beta + k_z \gamma) a],$$ derive the dispersion relation $E(k_x,k_y,k_z)$ of the crystal.
4. Find an expression for the group velocity $\mathbf{v}_{g}$ of the band.
5. Find an expression for the density of states _per unit volume_ in case of low $k=\sqrt{k_x^2+k_y^2+k_z^2}$.
6. What is the effective mass $m^*$ at $k_x=k_y=k_z=0$?

### Exercise 4: zincblende
The figure below shows a conventional unit cell of one of the possible crystal structures of zinc sulfide, which is also known as zincblende. The cubic conventional unit cell has a side length of $a$ and the sulfur atom is translated $(a,a,a)/4$ with respect to the zinc atom.

![plan-view-zb](https://upload.wikimedia.org/wikipedia/commons/c/c0/Sphalerite-unit-cell-3D-balls.png)

??? info "source"
    By Benjah-bmm27, own work, public domain, [link](https://commons.wikimedia.org/wiki/File:Sphalerite-unit-cell-3D-balls.png)

1. What type of lattice does zincblende have? Give a set of primitive lattice vectors of this lattice.
2. What is the basis of this crystal if we pick primitive lattice vectors? And what would be the basis if we pick conventional lattice vectors?
3. From the set of primitive lattice vectors, calculate the reciprocal lattice vectors.
4. Let the atomic form factor of zinc be $f_{Zn}$ and that of sulfur be $f_{S}$. Derive an expression for the structure factor of the primitive unit cell of the crystal.
5. What will happen to the diffraction pattern if $f_{Zn} = f_S$?

### Exercise 5: nearly free electrons in aluminium
One material that can be described particularly well with the nearly free electron model is aluminium. Aluminium has a fcc crystal structure, which means that the reciprocal space can be described with an bcc lattice. The first Brillouin zone of a fcc crystal is depicted in the figure below. We further set the edge length of the cubic conventional unit cell of the reciprocal bcc lattice to $2\pi/a$.

![BZ-fcc](https://upload.wikimedia.org/wikipedia/commons/c/c1/Brillouin_Zone_%281st%2C_FCC%29.svg)

??? info "source"
    By Inductiveload, own work, public domain, [link](https://commons.wikimedia.org/wiki/File:Brillouin_Zone_(1st,_FCC).svg).

1. Assuming free electrons, what is the lowest energy of the free electron waves at the $X$ and $L$ point in the figure above and what two wave functions do cross at each of these two points?

    In the nearly free electron model, the dispersion relation will be distorted at these 2 points. Due to the weak periodic potential of aluminium, it can be written as $$V(\mathbf{r})=\sum_{\mathbf{G}}V_{\mathbf{G}}e^{i \mathbf{G} \cdot \mathbf{r}},$$ where $\mathbf{G}$ is a reciprocal lattice vector and $V_{\mathbf{G}}$ is a Fourier component of the potential.

2. Find an expression of the 2×2 Hamiltonian of the lowest energy crossing at both the $X$ and $L$ point in the figure above.
3. Calculate the eigenvalues of the Hamiltonian at both points. What is the size of the band openings?
4. Show that the eigenstates at both points are consistent with Bloch's theorem and find expressions for $u_{n,\mathbf{k}}(\mathbf{r})$, where $\mathbf{k}$ is either the $X$ or $L$ point.
5. Make a sketch of the lowest energy band along the $L$-$\mathit{\Gamma}$-$X$ trajectory. Clearly indicate what the energies are at the $L$, $\mathit{\Gamma}$ and $X$ points.

### Exercise 6: a 2D semiconductor
Suppose we have a 2D semiconductor with a conduction band described by the dispersion relation $$E_{cb}(k_x, k_y)=E_G -2t_{cb} [\cos(k_x a)+\cos(k_ya)-2],$$ and a valence band described by
$$E_{vb}(k_x, k_y)=2t_{vb} [\cos(k_x a)+\cos(k_ya)-2].$$ 

1. Approximate both bands for low $k_x$, $k_y$ with a Taylor polynomial and express your result in terms of $k=\sqrt{k_x^2+k_y^2}$.
2. Derive an expression for the density of states *per unit area* for both approximated bands.
3. Assuming that $0 \ll \mu \ll E_G$ ($\mu$ is the chemical potential), derive an expression for the electron density in the conduction band as well as the hole density in the valence band at temperature $T$.
4. Derive an expression for $\mu$ in case of an intrinsic semiconductor.

    We now add doping to the semiconductor by adding $n_D$ donor atoms per unit area and $n_A$ acceptor atoms per unit area to the semiconductor.

5. Assume in this question that $t_{cb} = t_{vb} = t$. Derive an expression for $\mu$ of the semiconductor in the case of doping. You may assume that all the donor atoms are ionized and all acceptor atoms are occupied.